automx module

About
This module create a list of all the cars models sell in Mexico in the year 2018. The list are terms in a vocabulary.

The list is editable in the file modelosmexicoa.xlsx (Do the edit before install the module).

Installation
To install this module collow the general guide to install a module in Drupal 7.

Configuration
There is not admin configuration may be in the future could be.

Credit
The automx module was originally developed by medio y forma estudio and sponsor by Agencia molecula to use in the site seminuevosalonso.com.mx